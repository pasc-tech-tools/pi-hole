#!/bin/bash

pihole disable
service lighttpd stop
certbot renew --cert-name home.edouard.tech
service lighttpd start
pihole enable

# Detech the highest number of cert existing in archive and increment it by one
cert_files=(/etc/letsencrypt/archive/home.edouard.tech/cert*.pem)
echo "The following cert files were found: ${cert_files}"
max_cert_number=0

for cert_file in "${cert_files[@]}"; do
    cert_number=$(echo "$cert_file" | sed -e 's/.*cert\([0-9]*\).pem/\1/')
    if [ "$cert_number" -gt "$max_cert_number" ]; then
        max_cert_number="$cert_number"
    fi
done

echo "The highest issued cert number is: $max_cert_number"


cp "/etc/letsencrypt/archive/home.edouard.tech/fullchain$max_cert_number.pem" /home/ubuntu/homeassistant/fullchain.pem
cp "/etc/letsencrypt/archive/home.edouard.tech/privkey$max_cert_number.pem" /home/ubuntu/homeassistant/privkey.pem

docker restart homeassistant